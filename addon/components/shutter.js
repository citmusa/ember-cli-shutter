import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'shutter',
    setupShutter: function(){

    }.on('didInsertElement'),

    teardownShutter: function(){
        this.get('shutter').destroy();
    }.on('willDestroyElement'),

});
