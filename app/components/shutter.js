import Ember from 'ember';

// Expose de component to allow applications to user the component
import Shutter from 'ember-cli-chutter/components/shutter';
export default Shutter;
