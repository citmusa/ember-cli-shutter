// This is where you can tie your addon’s bower dependencies into the client app so that
// they actually get installed.

module.exports = {
  normalizeEntityName: function() {}, // no-op since we're just adding dependencies

  afterInstall: function() {
    return this.addBowerPackageToProject('shutter'); // is a promise
  }
};
